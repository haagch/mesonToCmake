#!/usr/bin/env python3
import shutil
import sys
import subprocess
import os
import json

if len(sys.argv) < 3:
    print("Usage : {} projectname projectdirectory".format(sys.argv[0]))
    sys.exit()

projectname = sys.argv[1]
projectdir = sys.argv[2]

def getOutput(command, cwd):
    return subprocess.run(command, stdout=subprocess.PIPE, cwd=cwd).stdout

introspectsubdir = ".INTROSPECT"
introdirout = getOutput(["meson", introspectsubdir], projectdir)

builddir = projectdir + os.sep + introspectsubdir
print("Using path " + builddir + " for temporary files")

targetsjson = json.loads(getOutput(["meson", "introspect", "--targets"], builddir))
print("Targets: ")

print("Targets: ", end="")
targets = {}
for target in targetsjson:
    print(target["name"] + ", ", end="")
    targets[target["name"]] = {
        "id": target["id"],
        "type": target["type"],
        "filename": target["filename"]
    }
print()


def libTolinkarg(lib):
    filename: str = os.path.split(lib)[-1]
    findEnd = len(filename) - 1
    while (filename[findEnd].isdigit() or filename[findEnd] == "."):
        findEnd -= 1
    l = filename[3:findEnd - 2]
    #print(lib, " -> ", l)
    return l

skipTargets = []

print("Getting files for target...")
includedirs = set()
#print(targets)
for target, vals in targets.items():
    print(target + ": ", end="")
    try:
        targetfiles = list(json.loads(getOutput(["meson", "introspect", "--target-files", vals["id"]], builddir)))
    except Exception as e:
        print("Skipping target: {} because of {}".format(target, e))
        skipTargets.append(target)
        continue
    print(targetfiles)
    targets[target]["files"] = targetfiles

    print("commands:")
    ninjat = getOutput(["ninja", "-t", "commands", vals["filename"]], builddir).decode().split(" ")  # HOPE USER DOES NOT USE ANY PATH WITH SPACES

    linkpaths = []
    libs = ["m"]  # often useful, just link it to everything
    defines = set()
    linkerargs = []
    for ninjao in ninjat:
        if "@" in ninjao:  # HOPE USER DOES NOT USE ANYTHING WITH @
            continue
        elif ninjao.startswith("-I"):
            includedirs.add(ninjao[2:])
        elif ninjao.startswith("-L"):
            linkpaths.append(ninjao)
        elif ninjao.startswith("-Wl"):
            linkerargs.append(ninjao)
        elif ".so" in ninjao:
            libs.append(libTolinkarg(ninjao))
        elif ninjao.startswith("-l"):
            libs.append(ninjao[2:])
        elif ninjao.startswith("-pthread"):
            targets[target]["pthread"] = True
        elif ninjao.startswith("-D") or ninjao.startswith("'-D"):
            defines.add(ninjao.lstrip('\'').rstrip('\''))
        else:
            print("Don't know what to do with", ninjao)

    #print("linkpaths", linkpaths)
    #print("linkerargs", linkerargs)
    #print("libs", libs)
    targets[target]["linklibs"] = libs
    targets[target]["defines"] = list(defines)

print("includes", includedirs)


print("Deleting path " + builddir)
#shutil.rmtree(builddir, ignore_errors=True)


with open("CMakeLists.txt", "w") as f:
    f.write("""
cmake_minimum_required(VERSION 2.8)

project({} C CXX)
#set(CMAKE_C_FLAGS "-std=c99")
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
include_directories(
""".format(projectname))

    for include in includedirs:
        if include.startswith("/"):  # HOPE USER ONLY USES THIS ON *NIX
            i = include
        else:
            i = "${{CMAKE_CURRENT_LIST_DIR}}/{}".format(include)
        f.write("    \"{}\"\n".format(i))
    f.write(")\n\n")

    for target, vals in targets.items():
        if target in skipTargets:
            continue
        if vals["type"] == "shared library":
            f.write("add_library({targetname} SHARED\n".format(targetname=target))
        elif vals["type"] == "executable":
            f.write("add_executable({targetname}\n".format(targetname=target))
        else:
            print("target type {} not supported yet".format(vals["type"]))
            continue
        for sourcefile in vals["files"]:
            f.write("    \"${{CMAKE_CURRENT_LIST_DIR}}/{sourcefile}\"\n".format(sourcefile=sourcefile))
        f.write(")\n")
        if "pthread" in vals and vals["pthread"]:
            f.write('target_compile_options({targetname} PUBLIC "-pthread")\n'.format(targetname=target))
        if len(vals["defines"]) > 0:
            f.write('target_compile_definitions({targetname} PUBLIC '.format(targetname=target))
            for define in vals["defines"]:
                f.write(define + " ")
            f.write(")\n")
        f.write("target_link_libraries({targetname} PRIVATE\n".format(targetname=target))
        for linklib in vals["linklibs"]:
            if linklib in targets.keys() and targets[linklib]["type"] == "executable":
                print("not linking executable target {}".format(linklib))
                continue
            f.write("    {lib}\n".format(lib=linklib))
        f.write(")\n\n")

print("Generated CMakeLists.txt. Now try running cmake.")
print("Remember that not all meson features are supported. You may have to manually edit your CMake files a little bit.")
